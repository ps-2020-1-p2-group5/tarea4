all: bin/estatic bin/dinamic
	echo "se crearon bin/estatic bin/dinamic"

bin/estatic: libreria/libstatic.a obj/main.o obj/lib.o
	gcc -Wall -static -L .libreria/libstatic obj/main.o obj/lib.o -o bin/static

bin/dinamic: libreria/libdinamic.so obj/main.o obj/lib.o
	gcc -Wall -L .libreria/libdinamic obj/main.o obj/lib.o -o bin/dinamic

libreria/libstatic.a: obj/lib.o
	ar rcs libreria/libstatic.a obj/lib.o

libreria/libdinamic.so: obj/lib.o
	gcc -shared -fPIC obj/lib.o -o libreria/libdinamic.so
	export LD_LIBRARY_PATH=libdinamic:$LD_LIBRARY_PATH

obj/main.o: src/main.c
	gcc -Wall -c src/main.c -o obj/main.o

obj/lib.o: src/lib.c
	gcc -Wall -c src/lib.c -o obj/lib.o

clean:
	rm bin/* obj/* libreria/*
