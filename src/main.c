#include<stdio.h>
#include<getopt.h>
#include<stdlib.h>
#include "../include/lib.h"

void print_uso_f(){
	printf("Uso: utilfecha -s <segundos> | utilfecha -d <dias> \n");
	exit(2);
}

int main (int argc, char **argv){
	if (argc < 2){
		print_uso_f();
	}
	int option;
	int dflag= 0;
	int sflag= 0;
	while ((option = getopt(argc, argv, "d:s:")) !=-1) {
		switch (option) {
			case 'd' :
				if (dflag){
					print_uso_f();
					exit(1);
				} else {
					dflag++;
					sflag++;
				}
				utilfecha_d(atof(optarg));
				break;
		       case 's' :
				if (sflag){
			 		print_uso_f();
	 				exit(1);
	 			} else{ 
					sflag++;
					dflag++;
				}
			       	utilfecha_s(atof(optarg));
				break;
		}
	}
}
