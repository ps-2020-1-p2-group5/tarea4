#include "lib.h"
//convierte dias a años, meses, dìas
void utilfecha_d(int dia){
        double dias= (double) dia;
        double calc_a=dias/365;
        int anos= (int) calc_a;
        double calc_m = (calc_a-anos)*12;
        int meses=(int) calc_m;
        double calc_d = (calc_m-meses)*30;
        int d=(int) calc_d;
        printf("\nAnos: %d \nMeses: %d \nDias: %d \n",anos,meses,d);
return;
}

//convierte segundos a horas, minutos, segundos
void utilfecha_s(int seg){
        double segun= (double) seg;
        double calc_h=segun/3600;
        int horas= (int) calc_h;
        double calc_m = (calc_h-horas)*60;
        int minutos=(int) calc_m;
        double calc_s = (calc_m-minutos)*60;
        int segundos=(int) calc_s;
        printf("\nHoras: %d \nMinutos: %d \nSegundos: %d \n",horas,minutos,segundos);
return;
}

